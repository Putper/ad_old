namespace AD
{
    public partial class MyStack<T> : IMyStack<T>
    {
        private IMyLinkedList<T> list;

        public MyStack()
        {
            list = new MyLinkedList<T>();
        }

        public bool IsEmpty()
        {
            return list.Size() == 0;
        }

        public T Pop()
        {
            try
            {
                T data = Top();
                list.RemoveFirst();
                return data;
            }
            catch (MyLinkedListEmptyException)
            {
                throw new MyStackEmptyException();
            }
        }

        public void Push(T data)
        {
            list.AddFirst(data);
        }

        public T Top()
        {
            try
            {
                return list.GetFirst();
            }
            catch (MyLinkedListEmptyException)
            {
                throw new MyStackEmptyException();
            }
        }
    }
}
