using System;

namespace AD
{
    public partial class BinarySearchTree<T> : BinaryTree<T>, IBinarySearchTree<T>
        where T : System.IComparable<T>
    {

        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------

        public void Insert(T x)
        {
            BinaryNode<T> node = new BinaryNode<T>(x, null, null);
            // if root isn't set yet, it can just set the root as the node
            if (root != null)
                Insert(node, root);
            else
                root = node;
        }

        /// <summary>
        /// Insert nodeToInsert into currentNode
        /// </summary>
        /// <param name="nodeToInsert">the node that has to be inserted</param>
        /// <param name="currentNode">the current node to try and insert it into (for recursion)</param>
        private void Insert(BinaryNode<T> nodeToInsert, BinaryNode<T> currentNode)
        {
            // uhoh double key
            if (nodeToInsert.data.Equals(currentNode.data))
                throw new BinarySearchTreeDoubleKeyException();

            // higher numbers go to the right
            // smaller numbers to the left
            if (nodeToInsert.data.CompareTo(currentNode.data) > 0) // toInsert > currentNode
            {
                // if it's empty, just set the node
                // else continue the insert recursion
                if (currentNode.right == null)
                    currentNode.right = nodeToInsert;
                else
                    Insert(nodeToInsert, currentNode.right);
            }
            else
            {
                if (currentNode.left == null)
                    currentNode.left = nodeToInsert;
                else
                    Insert(nodeToInsert, currentNode.left);
            }
        }

        public T FindMin()
        {
            if (root == null)
                throw new BinarySearchTreeEmptyException();
            return FindMin(root).GetData();
        }

        public IBinaryNode<T> FindMin(IBinaryNode<T> node)
        {
            return (node.GetLeft() == null)
                ? node
                : FindMin(node.GetLeft());
        }

        public void RemoveMin()
        {
            // cant remove min from empty tree
            if (root == null)
                throw new BinarySearchTreeEmptyException();
            // if 1 element, set null
            if (root.left == null && root.right == null)
                root = null;
            else
                RemoveMin(root);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="node"></param>
        /// <returns>if the current node is the most left node</returns>
        public bool RemoveMin(BinaryNode<T> node)
        {
            // if is last, notify that its the last by returning true
            if (node.GetLeft() == null)
                return true;

            // if this is the second to last node
            // we set the left to null, removing the min
            bool nextNodeIsLastNode = RemoveMin(node.GetLeft());
            if (nextNodeIsLastNode)
                node.left = null;

            return false;
        }

        public void Remove(T x)
        {
            throw new System.NotImplementedException();
        }

        public string InOrder()
        {
            if (root == null)
                return "";

            return InOrder(root).Trim();
        }

        private string InOrder(BinaryNode<T> node)
        {
            string toReturn = "";
            if (node.left != null)
                toReturn += InOrder(node.left);

            toReturn += node.data.ToString() + " ";

            if (node.right != null)
                toReturn += InOrder(node.right);

            return toReturn;
        }

        public override string ToString()
        {
            return InOrder();
        }
    }
}
