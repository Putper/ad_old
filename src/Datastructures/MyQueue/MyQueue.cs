using System.Collections.Generic;

namespace AD
{
    public partial class MyQueue<T> : IMyQueue<T>
    {
        IMyLinkedList<T> list = new MyLinkedList<T>();

        public bool IsEmpty()
        {
            return list.Size() == 0;
        }

        public void Enqueue(T data)
        {
            list.Insert(list.Size(), data);
        }

        public T GetFront()
        {
            try
            {
                return list.GetFirst();
            }
            catch(MyLinkedListEmptyException)
            {
                throw new MyQueueEmptyException();
            }
        }

        public T Dequeue()
        {
            try
            {
                T data = GetFront();
                list.RemoveFirst();
                return data;
            }
            catch (MyLinkedListEmptyException)
            {
                throw new MyQueueEmptyException();
            }
        }

        public void Clear()
        {
            list.Clear();
        }

    }
}