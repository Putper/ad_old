﻿using System.Text;

namespace AD
{
    public partial class MyLinkedList<T> : IMyLinkedList<T>
    {
        public MyLinkedListNode<T> first;
        private int size;

        /// <summary>
        /// O(1)
        /// </summary>
        public MyLinkedList()
        {
            first = null;
            size = 0;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="data"></param>
        public void AddFirst(T data)
        {
            var newNode = new MyLinkedListNode<T>();
            newNode.data = data;
            newNode.next = first;
            first = newNode;
            size++;
            //first = new MyLinkedListNode<T>();
            //first.data = data;
            //first.next = null;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public T GetFirst()
        {
            if (first == null)
                throw new MyLinkedListEmptyException();
            return first.data;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        public void RemoveFirst()
        {
            if (size == 0)
                throw new MyLinkedListEmptyException();
            first = first.next;
            size--;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public int Size()
        {
            return size;
            //if (first == null)
            //    return 0;

            //int count = 1;
            //MyLinkedListNode<T> current = first;
            //while(current.next != null)
            //{
            //    count++;
            //    current = first.next;
            //}
            //return count;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        public void Clear()
        {
            first = null;
            size = 0;
        }

        /// <summary>
        /// O(N)
        /// </summary>
        /// <param name="index"></param>
        /// <param name="data"></param>
        public void Insert(int index, T data)
        {
            if(index < 0 || index > size)
                throw new MyLinkedListIndexOutOfRangeException();
            
            // create new node
            MyLinkedListNode<T> newNode = new MyLinkedListNode<T>();
            newNode.data = data;
            size++;

            // if list is empty it only needs to be inserted
            if (index == 0)
            {
                first = newNode;
                return;
            }

            // get node thats right before requested index
            MyLinkedListNode<T> current = first;
            for(int i=0; i<index-1; ++i)
            {
                current = current.next;
            }

            //squeeze new node inbetween other nodes
            if (current.next != null) newNode.next = current.next;
            current.next = newNode;
        }

        /// <summary>
        /// O()
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (size == 0)
                return "NIL";
            StringBuilder sb = new StringBuilder("[");

            MyLinkedListNode<T> currentNode = first;
            while(currentNode != null)
            {
                if(!currentNode.Equals(first))
                    sb.Append(",");
                sb.Append(currentNode.data);
                currentNode = currentNode.next;
            }
            sb.Append("]");
            return sb.ToString();
        }
    }
}