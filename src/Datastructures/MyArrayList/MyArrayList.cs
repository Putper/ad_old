﻿using System.Text;

namespace AD
{
    public partial class MyArrayList : IMyArrayList
    {
        private int[] data;
        private int size;

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="capacity"></param>
        public MyArrayList(int capacity)
        {
            data = new int[capacity];
            size = 0;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="n"></param>
        public void Add(int n)
        {
            if (size == data.Length)
                throw new MyArrayListFullException();

            if (n < 0 || n > Capacity())
                throw new MyArrayListIndexOutOfRangeException();

            data[size] = n;
            size++;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public int Get(int index)
        {
            if(index > size-1 || index < 0)
                throw new MyArrayListIndexOutOfRangeException();

            return data[index];
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <param name="index"></param>
        /// <param name="n"></param>
        public void Set(int index, int n)
        {
            if (index > size- 1 || index < 0)
                throw new MyArrayListIndexOutOfRangeException();

            data[index] = n;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public int Capacity()
        {
            return data.Length;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        /// <returns></returns>
        public int Size()
        {
            return size;
        }

        /// <summary>
        /// O(1)
        /// </summary>
        public void Clear()
        {
            //for (int i = 0; i < data.Length; ++i)
            //    data[i] = 0;
            size = 0;
        }

        /// <summary>
        /// O(N)
        /// </summary>
        /// <param name="n"></param>
        /// <returns></returns>
        public int CountOccurences(int n)
        {
            int count = 0;
            for(int i=0; i<size; ++i)
            {
                if (data[i] == n)
                    count++;
            }

            return count;
        }

        /// <summary>
        /// O(N)
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            if (size == 0)
                return "NIL";
            StringBuilder sb = new StringBuilder("[");
            for(int i=0; i<size; ++i)
            {
                // first number just needs to be entered, ex [3]
                // later numbers require a comma, ex [3, 5]
                if (i == 0)
                    sb.Append(data[i]);
                else
                {
                    sb.Append(",");
                    sb.Append(data[i]);
                }
            }
            sb.Append("]");
            return sb.ToString();
        }
    }
}
