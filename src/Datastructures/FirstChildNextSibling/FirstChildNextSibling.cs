using System;
using System.Text;

namespace AD
{
    public partial class FirstChildNextSibling<T> : IFirstChildNextSibling<T>
    {
        public FirstChildNextSiblingNode<T> root;

        public IFirstChildNextSiblingNode<T> GetRoot()
        {
            return root;
        }

        public int Size()
        {
            if (root == null)
                return 0;
            return root.Size();
        }

        //public int Size()
        //{
        //    Size(root);
        //}

        public int Size(FirstChildNextSiblingNode<T> node)
        {
            if (node == null)
                return 0;

            return 1 + Size(node.GetFirstChild()) + Size(node.GetNextSibling());
        }

        public void PrintPreOrder()
        {
            FirstChildNextSiblingNode<T> node = root;
            int spacing = 0;
            while(node != null)
            {
                // write current node
                Console.WriteLine(new string('\t', spacing) + node.GetData());

                // get siblings
                FirstChildNextSiblingNode<T> sibling = node.GetNextSibling();
                while(sibling != null)
                {
                    // write sibling
                    Console.WriteLine(new string('\t', spacing + 1) + sibling.GetData());
                    sibling = sibling.GetNextSibling();
                }

                node = node.GetFirstChild();
            }
        }

        public override string ToString()
        {
            if (root == null)
                return "NIL";

            return root.ToString();
        }
    }
}