using System;

namespace AD
{
    public partial class BinaryTree<T> : IBinaryTree<T>
    {
        public BinaryNode<T> root;

        //----------------------------------------------------------------------
        // Constructors
        //----------------------------------------------------------------------

        public BinaryTree()
        {
            root = null;
        }

        public BinaryTree(T rootItem)
        {
            root = new BinaryNode<T>(rootItem, null, null);
        }


        //----------------------------------------------------------------------
        // Interface methods that have to be implemented for exam
        //----------------------------------------------------------------------

        public BinaryNode<T> GetRoot()
        {
            return root;
        }

        public int Size()
        {
            return Size(root);
        }

        public int Size(BinaryNode<T> node)
        {
            if (node == null)
                return 0;
            return Size(node.left) + Size(node.right) + 1;
        }

        public int Height()
        {
            return IsEmpty()
                ? -1
                : Height(root) - 1;
        }

        public int Height(BinaryNode<T> node)
        {
            if (node == null)
                return 0;
            return Math.Max(
                Height(node.left),
                Height(node.right)
            ) + 1;
        }

        public void MakeEmpty()
        {
            root = null;
        }

        public bool IsEmpty()
        {
            return root == null;
        }

        public void Merge(T rootItem, BinaryTree<T> t1, BinaryTree<T> t2)
        {
            if (t1.root == t2.root && t1.root != null)
                throw new System.ArgumentException();

            // allocate new node
            root = new BinaryNode<T>(rootItem, t1.root, t2.root);

            // ensure that every node is in one tree
            if (this != t1)
                t1.root = null;
            if (this != t2)
                t2.root = null;
        }

        public string ToPrefixString()
        {
            if (root == null)
                return "NIL";

            return root.ToPrefixString();
        }

        public string ToInfixString()
        {
            return ToInfixString(root);
        }

        public string ToInfixString(BinaryNode<T> node)
        {
            if (node == null)
                return "NIL";

            string left = ToInfixString(node.left);
            string right = ToInfixString(node.right);
            return $"[ {left} {node.data} {right} ]";
        }

        public string ToPostfixString()
        {
            return ToPostfixString(root);
        }

        public string ToPostfixString(BinaryNode<T> node)
        {
            if (node == null)
                return "NIL";

            string left = ToPostfixString(node.left);
            string right = ToPostfixString(node.right);
            return $"[ {left} {right} {node.data} ]";
        }


        //----------------------------------------------------------------------
        // Interface methods : methods that have to be implemented for homework
        //----------------------------------------------------------------------

        /// <summary>
        /// count the number of leaves in this tree (starting from root)
        /// </summary>
        /// <returns></returns>
        public int NumberOfLeaves()
        {
            return NumberOfLeaves(root);
        }

        /// <summary>
        /// count the number of leaves for given node
        /// </summary>
        /// <param name="node"></param>
        /// <returns></returns>
        public int NumberOfLeaves(BinaryNode<T> node)
        {
            // null can't be a leaf
            if (node == null)
                return 0;
            // leaves are nodes without children
            if (node.left == null && node.right == null)
                return 1;
            // if the node has children, try counting those
            return NumberOfLeaves(node.left) + NumberOfLeaves(node.right);
        }

        public int NumberOfNodesWithOneChild()
        {
            return NumberOfNodesWithOneChild(root);
        }

        public int NumberOfNodesWithOneChild(BinaryNode<T> node)
        {
            // null can't have children
            if (node == null)
                return 0;

            int count = 0;
            // count children
            if (node.left != null)
                count += NumberOfNodesWithOneChild(node.left);
            if (node.right != null)
                count += NumberOfNodesWithOneChild(node.right);
            // count this node if it has one child
            if (node.left != null && node.right == null
                || node.left == null && node.right != null)
                count++;
            // if the node has children, try counting those
            return count;
        }

        public int NumberOfNodesWithTwoChildren()
        {
            return NumberOfNodesWithTwoChildren(root);
        }

        public int NumberOfNodesWithTwoChildren(BinaryNode<T> node)
        {
            // null can't have children
            if (node == null)
                return 0;

            int count = 0;
            // count children
            if (node.left != null)
                count += NumberOfNodesWithTwoChildren(node.left);
            if (node.right != null)
                count += NumberOfNodesWithTwoChildren(node.right);
            // count this node if it has one child
            if (node.left != null && node.right != null)
                count++;
            // if the node has children, try counting those
            return count;
        }
    }
}