using System.Collections.Generic;


namespace AD
{
    public partial class MergeSort : Sorter
    {

        public override void Sort(List<int> list)
        {
            RecursiveSort(list, 0, list.Count - 1);
        }

        public void RecursiveSort(List<int> list, int left, int right)
        {
            if (left >= right)
                return;

            int center = (left + right) / 2;
            RecursiveSort(list, left, center); // sort left
            RecursiveSort(list, center + 1, right); // sort right

            // merge
            int n1 = center - left + 1;
            int n2 = right - center;

            List<int> leftList = list.GetRange(left, n1);
            List<int> rightList = list.GetRange(center + 1, n2);

            int i = 0;
            int j = 0;
            int k = left;
            while (i < n1 && j < n2)
            {
                if (leftList[i] <= rightList[j])
                {
                    list[k] = leftList[i];
                    i++;
                }
                else
                {
                    list[k] = rightList[j];
                    j++;
                }
                k++;
            }

            while (i < n1)
            {
                list[k] = leftList[i];
                i++;
                k++;
            }

            while (j < n2)
            {
                list[k] = rightList[j];
                j++;
                k++;
            }
        }

        //    public override void Sort(List<int> list)
        //    {
        //        new InsertionSort().Sort(list);
        //        //RecursiveSort(list, 0, list.Count - 1);
        //        ////// sort
        //        //List<int> sortedList = RecursiveSort(list, 0, list.Count-1);
        //        //// move items to list
        //        //list.Clear();
        //        //foreach (int item in sortedList)
        //        //    list.Add(item);
        //    }

        //    private void RecursiveSort(List<int> list, int startIndex, int endIndex)
        //    {
        //        int size = endIndex - startIndex;
        //        // if list with one value, there's nothing to sort
        //        if (size <= 1)
        //            return;

        //        int half = size / 2;
        //        RecursiveSort(list, startIndex, startIndex + half);
        //        RecursiveSort(list, startIndex + half + 1, endIndex);

        //        // sort into sorted array
        //        List<int> sortedList = new List<int>();
        //        int half1Index = startIndex;
        //        int half2Index = startIndex + half;
        //        while(sortedList.Count < size)
        //        {
        //            // add first if it's lower, or if second number doesn't exist
        //            int value1 = list[half1Index];
        //            int value2 = list[half2Index];
        //            if (half1Index <= half && half2Index >= size || value1 < value2)
        //            {
        //                half1Index++;
        //                sortedList.Add(value1);
        //            }
        //            else
        //            {
        //                half2Index++;
        //                sortedList.Add(value2);
        //            }
        //        }
        //        foreach (int item in sortedList)
        //            System.Console.Write(item + " ");
        //        System.Console.WriteLine();

        //        // replace original with sorted array
        //        for (int i = 0; i < sortedList.Count; ++i)
        //            list[startIndex + i] = sortedList[i];
        //    }
        //}
    }
}
