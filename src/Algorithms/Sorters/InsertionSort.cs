using System.Collections.Generic;


namespace AD
{
    public partial class InsertionSort : Sorter
    {
        public override void Sort(List<int> list)
        {
            if (list.Count == 0)
                return;

            List<int> originalList = new List<int>(list);
            list.Clear();
            foreach(int item in originalList)
            {
                if (list.Count == 0)
                {
                    list.Add(item);
                    continue;
                }
                for(int i=0; i < list.Count; ++i)
                {
                    if(item < list[i])
                    {
                        list.Insert(i, item);
                        break;
                    }
                    else if(i == list.Count-1)
                    {
                        list.Add(item);
                        break;
                    }
                }
            }
        }
    }
}
