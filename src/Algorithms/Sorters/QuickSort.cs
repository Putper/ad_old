using System;
using System.Collections.Generic;

namespace AD
{
    public partial class QuickSort : Sorter
    {
        private static int CUTOFF = 3;

        public override void Sort(List<int> list)
        {
            if(list.Count <= 1)
                return;

            // get pivot
            var pivotList = new List<int>() { list[0], list[list.Count / 2], list[^1] };
            InsertionSort insertionSort = new InsertionSort();
            insertionSort.Sort(pivotList);
            int pivot = pivotList[1]; // pivot is centre

            pivot = list[list.Count / 2];

            // sort into smaller numbers,
            // same numbers and larger numbers
            List<int> smaller = new List<int>();
            List<int> same = new List<int>();
            List<int> larger = new List<int>();
            foreach (int item in list)
            {
                if (item < pivot)
                    smaller.Add(item);
                else if (item > pivot)
                    larger.Add(item);
                else
                    same.Add(item);
            }
            // Recursively sort each list
            Sort(smaller);
            Sort(larger);

            // merge together sorted parts
            list.Clear();
            list.AddRange(smaller);
            list.AddRange(same);
            list.AddRange(larger);
        }

        private static void Merge(int[] a, int[] tempArray, int leftPos, int rightPos, int rightEnd)
        {
            int leftEnd = rightPos - 1;
            int tempPos = leftPos;
            int elementsCount = rightEnd - leftPos + 1;

            while(leftPos <= leftEnd && rightPos <= rightEnd)
            {
                if (a[leftPos].CompareTo(a[rightPos]) <= 0)
                    tempArray[tempPos++] = a[leftPos++];
                else
                    tempArray[tempPos++] = a[rightPos++];
            }

            while (leftPos <= leftEnd)
                tempArray[tempPos++] = a[leftPos++];

            while (rightPos <= rightEnd)
                tempArray[tempPos++] = a[rightPos++];

            // copy tempArray back
            for (int i = 0; i < elementsCount; ++i, rightEnd--)
                a[rightEnd] = tempArray[rightEnd];
        }
    }
}
