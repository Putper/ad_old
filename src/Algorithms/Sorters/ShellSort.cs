using System.Collections.Generic;


namespace AD
{
    public partial class ShellSort : Sorter
    {
        private InsertionSort InsertionSort;
        public ShellSort()
        {
            InsertionSort = new InsertionSort();
        }
        public override void Sort(List<int> list)
        {
            if (list.Count <= 1)
                return;

            InternalShellSort(list, 5);
            InternalShellSort(list, 3);
            InsertionSort.Sort(list);
        }

        private void InternalShellSort(List<int> list, int increment)
        {
            // current offset
            for(int offset=0; offset<increment; ++offset)
            {
                // how often we have to run it for this list
                // based on the current increment
                // (int division rounded up)
                int timesToRun = (list.Count - 1) / increment + 1;
                for (int timesRan=0; timesRan<timesToRun; ++timesRan)
                {
                    // get values and sort
                    int indexA = increment * timesRan + offset;
                    int indexB = increment * (timesRan + 1) + offset;
                    SwapIfLarger(list, indexA, indexB);
                }
            }
        }
        
        /// <summary>
        /// Will swap two values in a given list
        /// if indexA's value is bigger than indexB's value
        /// </summary>
        /// <param name="list"></param>
        /// <param name="index1"></param>
        /// <param name="index2"></param>
        private void SwapIfLarger(List<int> list, int indexA, int indexB)
        {
            // prevent IndexOutOfRange Exception
            if (indexA > list.Count-1 || indexB > list.Count-1)
                return;
            if (list[indexA] > list[indexB])
            {
                int valueA = list[indexA];
                list[indexA] = list[indexB];
                list[indexB] = valueA;
            }
        }
    }
}
